"""Model definition for movie genres tagging."""

import torch
import torch.nn as nn
import pytorch_lightning as pl
from transformers import BertModel, AdamW, get_linear_schedule_with_warmup
from pytorch_lightning.metrics.functional import auroc


class MovieGenresTagger(pl.LightningModule):
    """Bert-based model with an extra fully connected layer."""

    def __init__(self, label_columns, pretrained_model='bert-base-cased',
                 weights=None, n_training_steps=None, n_warmup_steps=None):
        super().__init__()
        self.bert = BertModel.from_pretrained(
            pretrained_model, return_dict=True)
        self.label_columns = label_columns
        self.classifier = nn.Linear(
            self.bert.config.hidden_size, len(self.label_columns))
        self.n_training_steps = n_training_steps
        self.n_warmup_steps = n_warmup_steps
        self.criterion = nn.BCELoss(reduction='none')
        if weights:
            self.weights = torch.Tensor(weights).cuda()
        else:
            self.weights = weights

    def forward(self, input_ids, attention_mask, labels=None):
        """Apply a forward pass through the network architecture."""
        output = self.bert(input_ids, attention_mask=attention_mask)
        output = self.classifier(output.pooler_output)
        output = torch.sigmoid(output)
        loss = 0
        if labels is not None:
            loss = self.criterion(output, labels)
            if self.weights is not None:
                loss = (loss * self.weights).mean()
            else:
                loss = loss.mean()
        return loss, output

    def training_step(self, batch, batch_idx):
        """Apply model and log training loss."""
        input_ids = batch["input_ids"]
        attention_mask = batch["attention_mask"]
        labels = batch["labels"]
        loss, outputs = self(input_ids, attention_mask, labels)
        self.log("train_loss", loss, prog_bar=True, logger=True)
        return {"loss": loss, "predictions": outputs, "labels": labels}

    def validation_step(self, batch, batch_idx):
        """Apply model and log validation loss."""
        input_ids = batch["input_ids"]
        attention_mask = batch["attention_mask"]
        labels = batch["labels"]
        loss, outputs = self(input_ids, attention_mask, labels)
        self.log("val_loss", loss, prog_bar=True, logger=True)
        return loss

    def test_step(self, batch, batch_idx):
        """Apply model and log test loss."""
        input_ids = batch["input_ids"]
        attention_mask = batch["attention_mask"]
        labels = batch["labels"]
        loss, outputs = self(input_ids, attention_mask, labels)
        self.log("test_loss", loss, prog_bar=True, logger=True)
        return loss

    def training_epoch_end(self, outputs):
        """Apply performance metrics at the end of a training epoch."""
        labels = []
        predictions = []
        for output in outputs:
            for out_labels in output["labels"].detach().cpu():
                labels.append(out_labels)
            for out_predictions in output["predictions"].detach().cpu():
                predictions.append(out_predictions)

        labels = torch.stack(labels).int()
        predictions = torch.stack(predictions)

        for i, name in enumerate(self.label_columns):
            class_roc_auc = auroc(predictions[:, i], labels[:, i])
            self.logger.experiment.add_scalar(
                f"{name}_roc_auc/Train", class_roc_auc, self.current_epoch)

    def configure_optimizers(self):
        """Configure optimizer to adapt learning rate during training."""
        optimizer = AdamW(self.parameters(), lr=2e-5)

        scheduler = get_linear_schedule_with_warmup(
            optimizer,
            num_warmup_steps=self.n_warmup_steps,
            num_training_steps=self.n_training_steps
        )

        return dict(
            optimizer=optimizer,
            lr_scheduler=dict(
                scheduler=scheduler,
                interval='step'
            )
        )
