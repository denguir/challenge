"""Package for movie genres tagging."""

from .challenge import app


__all__ = ['app']
