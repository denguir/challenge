"""Data module used to iterate over the dataset."""

import torch
import pytorch_lightning as pl
from torch.utils.data import Dataset, DataLoader


class MovieGenresDataset(Dataset):
    """Dataset that allows parsing and transforming raw data."""

    def __init__(self, data, label_columns, tokenizer, max_token_len):
        self.tokenizer = tokenizer
        self.data = data
        self.label_columns = label_columns
        self.max_token_len = max_token_len

    def __len__(self):
        """Length of Dataset object."""
        return len(self.data)

    def __getitem__(self, index: int):
        """Access and encode next index data."""
        data_row = self.data.iloc[index]

        synopsis = data_row.synopsis
        labels = data_row[self.label_columns]

        encoder = self.tokenizer.encode_plus(
            synopsis,
            add_special_tokens=True,
            max_length=self.max_token_len,
            return_token_type_ids=False,
            padding="max_length",
            truncation=True,
            return_attention_mask=True,
            return_tensors='pt',
        )

        return dict(
            movie_id=data_row.movie_id,
            synopsis=synopsis,
            input_ids=encoder["input_ids"].flatten(),
            attention_mask=encoder["attention_mask"].flatten(),
            labels=torch.FloatTensor(labels)
        )


class MovieGenresDataModule(pl.LightningDataModule):
    """Data module that wraps datasets into a Dataloader."""

    def __init__(self, train_df, test_df, label_columns,
                 tokenizer, batch_size=8, max_token_len=128):
        super().__init__()
        self.batch_size = batch_size
        self.train_df = train_df
        self.test_df = test_df
        self.label_columns = label_columns
        self.tokenizer = tokenizer
        self.max_token_len = max_token_len

    def setup(self, stage=None):
        """Build every Dataset from raw data."""
        self.train_dataset = MovieGenresDataset(
            self.train_df,
            self.label_columns,
            self.tokenizer,
            self.max_token_len
        )

        self.test_dataset = MovieGenresDataset(
            self.test_df,
            self.label_columns,
            self.tokenizer,
            self.max_token_len
        )

    def train_dataloader(self):
        """Wrap train dataset into a data loader."""
        return DataLoader(
            self.train_dataset,
            batch_size=self.batch_size,
            shuffle=True,
            num_workers=16
        )

    def val_dataloader(self):
        """Wrap validation dataset into a data loader."""
        return DataLoader(
            self.test_dataset,
            batch_size=self.batch_size,
            num_workers=16
        )

    def test_dataloader(self):
        """Wrap test dataset into a data loader."""
        return DataLoader(
            self.test_dataset,
            batch_size=self.batch_size,
            num_workers=16
        )
