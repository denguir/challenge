"""Training script for movie genres tagging."""

from pytorch_lightning.loggers import TensorBoardLogger
from pytorch_lightning.callbacks import ModelCheckpoint, EarlyStopping
from .MovieGenresTagger import MovieGenresTagger
from .MovieGenresDataModule import MovieGenresDataModule
from transformers import BertTokenizerFast as BertTokenizer
from sklearn.model_selection import train_test_split
from .utils import mle_genres, get_label_columns, get_loss_weight
import pytorch_lightning as pl
import pandas as pd
import torch
import os
os.environ["TOKENIZERS_PARALLELISM"] = "false"


# Model hyper-parameters
N_EPOCHS = 10
BATCH_SIZE = 12
BERT_MODEL_NAME = 'bert-base-cased'
MAX_TOKEN_COUNT = 256


def train(df):
    """Train Bert model on df to predict movie genres from synopsis."""
    df = mle_genres(df)
    label_columns = get_label_columns(df)
    loss_weight = get_loss_weight(df)

    train_df, val_df = train_test_split(df, test_size=0.2)
    tokenizer = BertTokenizer.from_pretrained(BERT_MODEL_NAME)

    data_module = MovieGenresDataModule(
        train_df,
        val_df,
        label_columns,
        tokenizer,
        batch_size=BATCH_SIZE,
        max_token_len=MAX_TOKEN_COUNT
    )

    steps_per_epoch = len(train_df) // BATCH_SIZE
    total_training_steps = steps_per_epoch * N_EPOCHS
    warmup_steps = total_training_steps // 5
    warmup_steps, total_training_steps

    model = MovieGenresTagger(
        label_columns=label_columns,
        pretrained_model=BERT_MODEL_NAME,
        weights=loss_weight,
        n_training_steps=total_training_steps,
        n_warmup_steps=warmup_steps,
    )

    checkpoint_callback = ModelCheckpoint(
        dirpath="checkpoints",
        filename="best-checkpoint",
        save_top_k=1,
        verbose=True,
        monitor="val_loss",
        mode="min"
    )

    early_stopping_callback = EarlyStopping(monitor='val_loss', patience=3)
    logger = TensorBoardLogger("logs", name="movie-genres")

    trainer = pl.Trainer(
        # resume_from_checkpoint='checkpoints/best-checkpoint.ckpt',
        logger=logger,
        callbacks=[checkpoint_callback, early_stopping_callback],
        max_epochs=N_EPOCHS,
        gpus=1,
        progress_bar_refresh_rate=30
    )

    # save hyper parameters
    torch.save({
        "n_epochs": N_EPOCHS,
        "batch_size": BATCH_SIZE,
        "pretrained_model": BERT_MODEL_NAME,
        "tokenizer_model": BERT_MODEL_NAME,
        "max_token_count": MAX_TOKEN_COUNT,
        "label_columns": label_columns,
        "loss_weight": loss_weight
    }, "checkpoints/best-checkpoint-hparams.ckpt")

    # train
    trainer.fit(model, data_module)


if __name__ == '__main__':
    df = pd.read_csv('train.csv')
    train(df)
