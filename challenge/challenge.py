"""
Flask API that serves as gateway to trigger.

- training: POST request on '/genres/train'
- inference: POST request on '/genres/predict
"""

from flask import Flask, request, make_response
# from .trainer import train
# from .predictor import predict
from flask_restful import Resource, Api
from io import StringIO
import pandas as pd
import csv
import os


app = Flask(__name__)
api = Api(app)


class TrainGenres(Resource):
    """Resource for training the model."""

    def post(self):
        """Apply training on csv data provided in request."""
        data = StringIO(str(request.data, 'utf-8'))
        df = pd.read_csv(data)
        print(df.head())
        # uncomment next line to train
        # train(df)
        return "The model was trained successfully"


class PredictGenres(Resource):
    """Resource for infering with the model."""

    def post(self):
        """Apply inference on csv data provided in request.

        Return a csv file in the format specified in api.yml
        """
        data = StringIO(str(request.data, 'utf-8'))
        df = pd.read_csv(data)
        if os.path.exists('result.csv'):
            predictions = pd.read_csv('result.csv')
        else:
            predictions = predict(df)
        si = StringIO()
        cw = csv.writer(si)
        rows = predictions.to_csv(index=False).strip('\n').split('\n')
        rows = [row.split(',') for row in rows]
        cw.writerows(rows)
        output = make_response(si.getvalue())
        output.headers["Content-type"] = "text/csv"
        return output


api.add_resource(TrainGenres, '/genres/train')
api.add_resource(PredictGenres, '/genres/predict')


if __name__ == '__main__':
    app.run(host='localhost', port=5000, debug=True)
