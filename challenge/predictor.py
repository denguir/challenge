"""Inference script for movie genres tagging."""

import torch
import pandas as pd
from tqdm import tqdm
from .MovieGenresTagger import MovieGenresTagger
from transformers import BertTokenizerFast as BertTokenizer


def load_model_and_hparams():
    """Load saved model and hyper parameters from checkpoints directory."""
    hparams = torch.load("checkpoints/best-checkpoint-hparams.ckpt")
    model = MovieGenresTagger.load_from_checkpoint(
        'checkpoints/best-checkpoint.ckpt',
        label_columns=hparams["label_columns"])
    model.eval()
    model.freeze()
    return model, hparams


def predict(df_test):
    """Apply Bert model on df_test to predict movie genres from synopsis."""
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    model, hparams = load_model_and_hparams()
    model = model.to(device)

    bert_model_name = hparams['tokenizer_model']
    max_token_count = hparams['max_token_count']
    label_columns = hparams['label_columns']
    tokenizer = BertTokenizer.from_pretrained(bert_model_name)

    df_res = pd.DataFrame(columns=['movie_id', 'predicted_genres'])
    for _, row in tqdm(df_test.iterrows()):
        encoder = tokenizer.encode_plus(
            row.synopsis,
            add_special_tokens=True,
            max_length=max_token_count,
            return_token_type_ids=False,
            padding="max_length",
            return_attention_mask=True,
            return_tensors='pt',
        )
        input_ids = encoder["input_ids"][:, :max_token_count].to(device)
        attention_mask = encoder["attention_mask"][:,
                                                   :max_token_count].to(device)

        _, prediction = model(input_ids=input_ids,
                              attention_mask=attention_mask)
        prediction = prediction.flatten().cpu().numpy()
        top_5_idx = prediction.argsort()[-5:][::-1]
        top_5_tags = ' '.join([label_columns[i] for i in top_5_idx])
        df_res = df_res.append(
            {'movie_id': row.movie_id,
             'predicted_genres': top_5_tags},
            ignore_index=True)
    return df_res


if __name__ == '__main__':
    df = pd.read_csv('test.csv')
    res = predict(df)
    print(res.head())
