"""Utils functions used for data pre-processing."""

import pandas as pd
from sklearn.preprocessing import MultiLabelBinarizer


def mle_genres(df):
    """Add a binary label for each tag specified in genres column."""
    genres = [x.split(' ') for x in list(df['genres'])]
    mlb = MultiLabelBinarizer()
    y = mlb.fit_transform(genres)
    y = pd.DataFrame(y, columns=mlb.classes_)
    df = pd.concat([df, y], axis=1)
    return df


def get_label_columns(df):
    """Extract label columns from dataframe."""
    return df.columns.tolist()[4:]


def get_loss_weight(df):
    """Compute a weight vector to deal with data imbalance."""
    label_columns = get_label_columns(df)
    min_label_count = min(df[label_columns].sum())
    return (min_label_count / df[label_columns].sum()).tolist()
